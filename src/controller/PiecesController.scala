package controller
import scala.collection.JavaConverters._
import models.objects.Piece
import scala._
/**
  * Created by ste on 18/03/17.
  */
class PiecesController {
  var elements: List[Piece] = List()

  def createPieces = {
    elements = List(
      new Piece(402, 145),
      new Piece(1202, 140),
      new Piece(1272, 95),
      new Piece(1342, 40),
      new Piece(1650, 145),
      new Piece(2650, 145),
      new Piece(3000, 135),
      new Piece(3400, 125),
      new Piece(4200, 145),
      new Piece(4600, 40)
    )
  }

  def reset = this.elements = Nil

  def getElements: java.util.List[Piece] = elements.asJava

  def remove(index: Int) =  elements = elements.take(index) ++ elements.drop(index+1)

  def move =  elements foreach(el=>el.move())
}