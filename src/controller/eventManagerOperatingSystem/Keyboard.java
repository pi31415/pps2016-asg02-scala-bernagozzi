package controller.eventManagerOperatingSystem;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import controller.Main;
import utils.ResourcesConstants;
public class Keyboard implements KeyListener {

    private void setBackgroundPosition() {
        Main.scene.drawer().setBackground1PosX(-50);
        Main.scene.drawer().setBackground2PosX(750);
    }


    private void setMarioMoving(boolean toRight) {
        int direction = -1;
        if (toRight) {
            direction = 1;
        }
        Main.scene.characterController().mario().setMoving(true);
        Main.scene.characterController().mario().setToRight(toRight);
        Main.scene.moveController().setMovement(direction); // si muove verso sinistra
    }


    @Override
    public void keyPressed(KeyEvent e) {

        if(Main.scene.characterController().mario().isAlive() == true){
            if(e.getKeyCode() == KeyEvent.VK_RIGHT){

                // per non fare muovere il castello e start
                if(Main.scene.drawer().getxPos() == -1){
                    Main.scene.drawer().setxPos(0);
                    setBackgroundPosition();
                }
                setMarioMoving(true);
            }else if(e.getKeyCode() == KeyEvent.VK_LEFT){

                if(Main.scene.drawer().getxPos() == 4601){
                    Main.scene.drawer().setxPos(4600);
                    setBackgroundPosition();
                }
                setMarioMoving(false);
            }
            // salto
            if(e.getKeyCode() == KeyEvent.VK_UP){
                Main.scene.characterController().mario().setJumping(true);
                Audio.playSound(ResourcesConstants.AUDIO_JUMP);
            }

            if (e.getKeyCode() == KeyEvent.VK_R) {
                Main.scene.restart();
            }

        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        Main.scene.characterController().mario().setMoving(false);
        Main.scene.moveController().setMovement(0);
    }

    @Override
    public void keyTyped(KeyEvent e) {}

}
