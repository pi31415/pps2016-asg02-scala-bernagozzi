package controller.eventManagerOperatingSystem;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import java.util.logging.Logger;
import java.util.logging.Level;

public class Audio {
    private Clip clip;
    private static final Logger LOGGER = Logger.getLogger(Audio.class.getName());

    public Audio(String son) {

        try {
            AudioInputStream audio = AudioSystem.getAudioInputStream(getClass().getResource(son));
            clip = AudioSystem.getClip();
            clip.open(audio);
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "exception in playing audio file", e);
        }
    }

    public Clip getClip() {
        return clip;
    }

    public void play() {
        clip.start();
    }

    public void stop() {
        clip.stop();
    }

    public static void playSound(String son) {
        Audio s = new Audio(son);
        s.play();
    }
}
