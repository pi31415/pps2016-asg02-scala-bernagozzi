package controller;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Refresh implements Runnable {

    private final int PAUSE = 3;
    private static final Logger LOGGER = Logger.getLogger(Refresh.class.getName());

    public void run() {
        while (true) {
            Main.scene.repaint();
            try {
                Thread.sleep(PAUSE);
            } catch (InterruptedException e) {
                LOGGER.log(Level.WARNING, "exception in refreshing main scene", e);
            }
        }
    }

} 
