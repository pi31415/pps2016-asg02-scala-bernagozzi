package controller

import java.awt.Graphics
import java.awt.Graphics2D
import javax.swing._
import controller.eventManagerOperatingSystem.Audio
import controller.eventManagerOperatingSystem.Keyboard
import utils.Constants
import utils.ResourcesConstants
import view.Drawer

@SuppressWarnings(Array("serial")) class Controller() extends JPanel {
  var piecesController = new PiecesController
  var objectController = new ObjectController
  var characterController = new CharacterController
  var drawer = new Drawer
  var moveController = new MoveController
  drawer.initialization
  creation
  drawer.setCastleAndFlag
  setFocusAndKeyboard


  private def creation = {
    objectController.create
    piecesController.createPieces
    characterController.createCharacters
  }

  private def setFocusAndKeyboard = {
    this.setFocusable(true)
    this.requestFocusInWindow
    this.addKeyListener(new Keyboard)
  }

  def restart = {
    objectController.reset
    piecesController.reset
    characterController.deleteAll
    creation
    drawer.initialization
  }

  private def checkMario = {
    if (!characterController.checkMario) {
      restart
    }
  }

  private def checkEnd: Boolean = characterController.mario.getX > Constants.FLAG_X_POS


  override def paintComponent(g: Graphics): Unit = {
    if (!checkEnd) {
      checkMario
      super.paintComponent(g)
      val g2: Graphics = g.asInstanceOf[Graphics2D]
      characterController.contactsDetection(objectController, piecesController)
      moveController.moveFixedObjects(characterController, objectController, piecesController)
      drawer.drawImages(g2, characterController, objectController, piecesController)
      drawer.drawScore(g2, characterController.getScore)
    }
    else {
      Audio.playSound(ResourcesConstants.AUDIO_END)
    }
  }
}