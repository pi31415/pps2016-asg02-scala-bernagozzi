package controller
import scala.collection.JavaConverters._
import models.objects.Block
import models.objects.GameObject
import models.objects.Tunnel
import utils.Constants

/**
  * Created by ste on 18/03/17.
  */

class ObjectController {
  var elements: List[GameObject] = List()

  def create = {
    elements = List(
      new Tunnel(600, Constants.TUNNEL_POSITION_Y),
      new Tunnel(1000, Constants.TUNNEL_POSITION_Y),
      new Tunnel(1600, Constants.TUNNEL_POSITION_Y),
      new Tunnel(1900, Constants.TUNNEL_POSITION_Y),
      new Tunnel(2500, Constants.TUNNEL_POSITION_Y),
      new Tunnel(3000, Constants.TUNNEL_POSITION_Y),
      new Tunnel(3800, Constants.TUNNEL_POSITION_Y),
      new Tunnel(4500, Constants.TUNNEL_POSITION_Y),

      new Block(400, 180),
      new Block(1200, 180),
      new Block(1270, 170),
      new Block(1340, 160),
      new Block(2000, 180),
      new Block(2600, 160),
      new Block(2650, 180),
      new Block(3500, 160),
      new Block(3550, 140),
      new Block(4000, 170),
      new Block(4200, 200),
      new Block(4300, 210)
    )
  }

  def getElements: java.util.List[GameObject] = elements.asJava

  def reset = this.elements = Nil

  def move =  elements foreach(el=>el.move())
}