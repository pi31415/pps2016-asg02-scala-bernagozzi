package controller

import utils.Constants

/**
  * Created by ste on 19/03/17.
  */
class MoveController {
  def flipBetweenBackground1And2() = {
    if (Main.scene.drawer.getBackground1PosX == -Constants.IMAGE_WIDTH) {
      Main.scene.drawer.setBackground1PosX(Constants.IMAGE_WIDTH)
    }
    else if (Main.scene.drawer.getBackground2PosX == -Constants.IMAGE_WIDTH) {
      Main.scene.drawer.setBackground2PosX(Constants.IMAGE_WIDTH)
    }
    else if (Main.scene.drawer.getBackground1PosX == Constants.IMAGE_WIDTH) {
      Main.scene.drawer.setBackground1PosX(-Constants.IMAGE_WIDTH)
    }
    else if (Main.scene.drawer.getBackground2PosX == Constants.IMAGE_WIDTH) {
      Main.scene.drawer.setBackground2PosX(-Constants.IMAGE_WIDTH)
    }
  }

  def setMovement(direction: Int) = {
    Main.scene.drawer.setMov(direction)
  }

  private def moveTheBackgroundForWalking() = {
    Main.scene.drawer.setxPos(Main.scene.drawer.getxPos + Main.scene.drawer.getMov)
    Main.scene.drawer.setBackground1PosX(Main.scene.drawer.getBackground1PosX - Main.scene.drawer.getMov)
    Main.scene.drawer.setBackground2PosX(Main.scene.drawer.getBackground2PosX - Main.scene.drawer.getMov)
  }

  def updateBackgroundOnMovement() = {
    if (Main.scene.drawer.getxPos >= Constants.INITIAL_POSITION && Main.scene.drawer.getxPos <= Constants.FLAG_X_POS) {
      moveTheBackgroundForWalking
    }
    flipBetweenBackground1And2
  }

  def moveFixedObjects(characterController: CharacterController, objectController: ObjectController, piecesController: PiecesController) = {
    this.updateBackgroundOnMovement
    if (Main.scene.drawer.getxPos >= Constants.INITIAL_POSITION && Main.scene.drawer.getxPos <= Constants.FLAG_X_POS) {
      objectController.move
      piecesController.move
      characterController.move
    }
  }
}