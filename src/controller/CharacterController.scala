package controller
import scala.collection.JavaConversions._

import controller.eventManagerOperatingSystem.Audio
import models.characters.Mario
import models.characters.Mushroom
import models.characters.Turtle
import models.objects.Score
import utils.ResourcesConstants


/**
  * Created by ste on 18/03/17.
  */
class CharacterController {
  var mario: Mario = _
  var mushroom: Mushroom = _
  var turtle: Turtle = _
  var score: Score = _

  def createCharacters = {
    this.mario = new Mario(300, 245)
    this.mushroom = new Mushroom(800, 263)
    this.turtle = new Turtle(950, 243)
    this.score = new Score
  }

  private def detectContacCharacterObject(objects: ObjectController) = {
    for (obj <- objects getElements) {
      if (this.mario.isNearby(obj)) this.mario.contact(obj)
      if (this.mushroom.isNearby(obj)) this.mushroom.contact(obj)
      if (this.turtle.isNearby(obj)) this.turtle.contact(obj)
    }
  }

  def checkMario: Boolean  = mario isAlive

  private def detectContactsBetweenCharcters = {
    if (this.mushroom.isNearby(turtle)) {
      this.mushroom.contact(turtle)
    }
    if (this.turtle.isNearby(mushroom)) {
      this.turtle.contact(mushroom)
    }
    if (this.mario.isNearby(mushroom)) {
      this.mario.contact(mushroom)
    }
    if (this.mario.isNearby(turtle)) {
      this.mario.contact(turtle)
    }
  }

  private def detectContactMarioPieces(pieces: PiecesController) = {
    var i: Int = 0
    while (i < pieces.getElements.size) {
        if (this.mario.contactPiece(pieces.getElements.get(i))) {
          Audio.playSound(ResourcesConstants.AUDIO_MONEY)
          pieces.remove(i)
          this.score.increase()
        }
        i += 1
    }
  }

  def contactsDetection(objects: ObjectController, pieces: PiecesController) = {
    detectContacCharacterObject(objects)
    detectContactMarioPieces(pieces)
    detectContactsBetweenCharcters
  }

  def deleteAll = {
    this.mario = null
    this.mushroom = null
    this.turtle = null
    this.score.reset()
  }

  def getScore: Int = score getScore

  def move = {
    mushroom.move
    turtle.move
  }
}