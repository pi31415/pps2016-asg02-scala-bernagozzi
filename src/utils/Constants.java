package utils;

/**
 * Created by ste on 19/03/17.
 */
public class Constants {
    public static final int MARIO_FREQUENCY = 25;
    public static final int MUSHROOM_FREQUENCY = 45;
    public static final int TURTLE_FREQUENCY = 45;
    public static final int MUSHROOM_DEAD_OFFSET_Y = 20;
    public static final int TURTLE_DEAD_OFFSET_Y = 30;
    public static final int FLAG_X_POS = 4650;
    public static final int CASTLE_X_POS = 4850;
    public static final int FLAG_Y_POS = 115;
    public static final int CASTLE_Y_POS = 145;
    public static final int IMAGE_WIDTH = 800;
    public static final int INITIAL_POSITION = 0;
    public static final int OFFSET_SCORE_X = 10;
    public static final int OFFSET_SCORE_Y = 10;
    public static final int TUNNEL_POSITION_Y = 230;
    public static final int BACKGROUND_OFFSET_X = -50;
    public static final int BACKGROUND_OFFSET_Y = 0;
    public static final int MARIO_OFFSET_X = 300;
    public static final int MARIO_OFFSET_Y = 245;
}
