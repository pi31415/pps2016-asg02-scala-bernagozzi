package view

import controller.CharacterController
import controller.ObjectController
import controller.PiecesController
import models.objects.GameObject
import models.objects.Piece
import utils.Constants
import utils.ResourcesConstants
import utils.Utils
import java.awt._
import scala.collection.JavaConversions._


/**
  * Created by ste on 18/03/17.
  */
class Drawer {
  private var imgBackground1: Image = _
  private var imgBackground2: Image = _
  private var castle: Image = _
  private var start: Image = _
  private var imgFlag: Image = _
  private var imgCastle: Image = _
  private var background1PosX: Int = 0
  private var background2PosX: Int = 0
  private var mov: Int = 0
  private var xPos: Int = 0
  private var floorOffsetY: Int = 0
  private var heightLimit: Int = 0

  def setBackground2PosX(background2PosX: Int) = {
    this.background2PosX = background2PosX
  }

  def setBackground1PosX(x: Int) = {
    this.background1PosX = x
  }

  def setFloorOffsetY(floorOffsetY: Int) = {
    this.floorOffsetY = floorOffsetY
  }

  def setHeightLimit(heightLimit: Int) = {
    this.heightLimit = heightLimit
  }

  def setxPos(xPos: Int) = {
    this.xPos = xPos
  }

  def setMov(mov: Int) = {
    this.mov = mov
  }

  def getHeightLimit: Int = heightLimit

  def getFloorOffsetY: Int = floorOffsetY

  def getMov: Int = mov

  def getxPos: Int = xPos

  def getBackground1PosX: Int = background1PosX

  def getBackground2PosX: Int = background2PosX

  def drawScore(g2: Graphics, score: Int) = {
    g2.drawString(score + " Points", Constants.OFFSET_SCORE_X, Constants.OFFSET_SCORE_Y)
  }

  private def variablesInitialization = {
    this.background1PosX = -50
    this.background2PosX = 750
    this.mov = Constants.INITIAL_POSITION
    this.xPos = -1
    this.floorOffsetY = 293
    this.heightLimit = 0
  }

  def initialization = {
    variablesInitialization
    backgroundInitialization
  }

  private def drawTurtle(g2: Graphics, characterController: CharacterController) = {
    if (characterController.turtle.isAlive) g2.drawImage(characterController.turtle.walk(ResourcesConstants.IMGP_CHARACTER_TURTLE, Constants.TURTLE_FREQUENCY), characterController.turtle.getX, characterController.turtle.getY, null)
    else g2.drawImage(characterController.turtle.deadImage, characterController.turtle.getX, characterController.turtle.getY + Constants.TURTLE_DEAD_OFFSET_Y, null)
  }

  private def drawMushroom(g2: Graphics, characterController: CharacterController) = {
    if (characterController.mushroom.isAlive) g2.drawImage(characterController.mushroom.walk(ResourcesConstants.IMGP_CHARACTER_MUSHROOM, Constants.MUSHROOM_FREQUENCY), characterController.mushroom.getX, characterController.mushroom.getY, null)
    else g2.drawImage(characterController.mushroom.deadImage, characterController.mushroom.getX, characterController.mushroom.getY + Constants.MUSHROOM_DEAD_OFFSET_Y, null)
  }

  private def drawBackground(g2: Graphics) = {
    g2.drawImage(this.imgBackground1, this.background1PosX, 0, null)
    g2.drawImage(this.imgBackground2, this.background2PosX, 0, null)
    g2.drawImage(this.castle, 10 - this.xPos, 95, null)
    g2.drawImage(this.start, 220 - this.xPos, 234, null)
  }

  private def drawObjects(g2: Graphics, objectController: ObjectController) = {
    for (obj <- objectController.getElements) {
      g2.drawImage(obj.getImgObj, obj.getX, obj.getY, null)
    }
  }

  private def drawPieces(g2: Graphics, piecesController: PiecesController) = {
    for (piece <- piecesController.getElements) {
      g2.drawImage(piece.imageOnMovement, piece.getX, piece.getY, null)
    }
  }

  private def drawCastleAndFlag(g2: Graphics) = {
    g2.drawImage(this.imgFlag, Constants.FLAG_X_POS - this.xPos, Constants.FLAG_Y_POS, null)
    g2.drawImage(this.imgCastle, Constants.CASTLE_X_POS - this.xPos, Constants.CASTLE_Y_POS, null)
  }

  private def drawMarioMoving(g2: Graphics, characterController: CharacterController) = {
    if (characterController.mario.isJumping) g2.drawImage(characterController.mario.doJump, characterController.mario.getX, characterController.mario.getY, null)
    else g2.drawImage(characterController.mario.walk(ResourcesConstants.IMGP_CHARACTER_MARIO, Constants.MARIO_FREQUENCY), characterController.mario.getX, characterController.mario.getY, null)
  }

  def backgroundInitialization: Unit = {
    this.imgBackground1 = Utils.getImage(ResourcesConstants.IMG_BACKGROUND)
    this.imgBackground2 = Utils.getImage(ResourcesConstants.IMG_BACKGROUND)
    this.castle = Utils.getImage(ResourcesConstants.IMG_CASTLE)
    this.start = Utils.getImage(ResourcesConstants.START_ICON)
  }

  def setCastleAndFlag: Unit = {
    this.imgCastle = Utils.getImage(ResourcesConstants.IMG_CASTLE_FINAL)
    this.imgFlag = Utils.getImage(ResourcesConstants.IMG_FLAG)
  }

  def drawImages(g2: Graphics, characterController: CharacterController, objectController: ObjectController, piecesController: PiecesController): Boolean = {
    drawBackground(g2)
    drawObjects(g2, objectController)
    drawPieces(g2, piecesController)
    drawCastleAndFlag(g2)
    drawMarioMoving(g2, characterController)
    drawMushroom(g2, characterController)
    drawTurtle(g2, characterController)
  }
}