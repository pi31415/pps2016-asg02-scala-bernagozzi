package models.objects;

import utils.ResourcesConstants;
import utils.Utils;

public class Tunnel extends GameObject {

    public static final int WIDTH = 43;
    public static final int HEIGHT = 65;

    public Tunnel(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
        super.imgObj = Utils.getImage(ResourcesConstants.IMG_TUNNEL);
    }

}
