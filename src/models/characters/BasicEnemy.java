package models.characters;

import controller.Main;
import models.objects.GameObject;

import java.awt.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.sun.xml.internal.ws.spi.db.BindingContextFactory.LOGGER;

/**
 * Created by ste on 18/03/2017.
 */
public abstract class BasicEnemy extends BasicCharacter implements Runnable{

    
    
    protected Image img;
    protected final int PAUSE = 25;
    protected int direction;

    private static final Logger LOGGER = Logger.getLogger(BasicEnemy.class.getName());

    public BasicEnemy(int x, int y, int width, int height) {
        super(x,y,width,height);
        this.setToRight(true);
        this.setMoving(true);
        this.direction = 1;
    }


    protected void setDirection(boolean toRight) {
        this.direction = -1;
        if(toRight) {
            this.direction = 1;
        }
        this.setToRight(toRight);
    }

    //getters
    public Image getImg() {
        return img;
    }

    @Override
    public void move() {
        this.direction = isToRight() ? 1 : -1;
        if(this.isAlive()) {
            if (Main.scene.characterController().mario().isMoving()) {
                if (Main.scene.characterController().mario().isToRight()) {
                    if (this.isToRight()) {
                        this.setX(this.getX());
                    } else {
                        this.setX(this.getX() + (this.direction * 2));
                    }
                } else {
                    if (this.isToRight()) {
                        this.setX(this.getX() + (this.direction * 2));
                    } else {
                        this.setX(this.getX());
                    }
                }
            } else {
                this.setX(this.getX() + this.direction);
            }
        } else {
            if (Main.scene.characterController().mario().isMoving()) {
                int marioDirection = Main.scene.characterController().mario().isToRight() ? 1 : -1;
                    this.setX(this.getX() - marioDirection);

            } else {
                this.setX(this.getX());
            }
        }
    }

    @Override
    public void run() {
        while (true) {
            if (this.alive) {
                this.move();
                try {
                    Thread.sleep(PAUSE);
                } catch (InterruptedException e) {
                    LOGGER.log(Level.WARNING, "exception in sleeping basic enemy", e);
                }
            }
        }
    }

    public void contact(GameObject obj) {
        if (this.hitAhead(obj) && this.isToRight()) {
            setDirection(false);
        } else if (this.hitBack(obj) && !this.isToRight()) {
            setDirection(true);
        }
    }

    public void contact(BasicCharacter pers) {
        if (this.hitAhead(pers) && this.isToRight()) {
            setDirection(false);
        } else if (this.hitBack(pers) && !this.isToRight()) {
            setDirection(true);
        }
    }

}
