package models.characters;

import java.awt.Image;

import controller.Main;
import models.objects.GameObject;
import utils.ResourcesConstants;
import utils.Utils;

public class BasicCharacter implements Character {

    public static final int PROXIMITY_MARGIN = 10;
    private int width, height;
    private int x, y;
    protected boolean moving;
    protected boolean toRight;
    public int counter;
    protected boolean alive;

    public BasicCharacter(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.height = height;
        this.width = width;
        this.counter = 0;
        this.moving = false;
        this.toRight = true;
        this.alive = true;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getCounter() {
        return counter;
    }

    public boolean isAlive() {
        return alive;
    }

    public boolean isMoving() {
        return moving;
    }

    public boolean isToRight() {
        return toRight;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setMoving(boolean moving) {
        this.moving = moving;
    }

    public void setToRight(boolean toRight) {
        this.toRight = toRight;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public Image walk(String name, int frequency) {
        String str = ResourcesConstants.IMG_BASE + name + (!this.moving || ++this.counter % frequency == 0 ? ResourcesConstants.IMGP_STATUS_ACTIVE : ResourcesConstants.IMGP_STATUS_NORMAL) +
                (this.toRight ? ResourcesConstants.IMGP_DIRECTION_DX : ResourcesConstants.IMGP_DIRECTION_SX) + ResourcesConstants.IMG_EXT;
        return Utils.getImage(str);
    }

    public void move() {
        if (Main.scene.drawer().getxPos() >= 0) {
            this.x = this.x - Main.scene.drawer().getMov();
        }
    }

    public boolean hitAhead(GameObject og) {
        return !(this.x + this.width < og.getX() || this.x + this.width > og.getX() + 5 ||
                this.y + this.height <= og.getY() || this.y >= og.getY() + og.getHeight());
    }

    protected boolean hitBack(GameObject og) {
        return !(this.x > og.getX() + og.getWidth() || this.x + this.width < og.getX() + og.getWidth() - 5 ||
                this.y + this.height <= og.getY() || this.y >= og.getY() + og.getHeight());
    }

    protected boolean hitBelow(GameObject og) {
        return !(this.x + this.width < og.getX() + 5 || this.x > og.getX() + og.getWidth() - 5 ||
                this.y + this.height < og.getY() || this.y + this.height > og.getY() + 5);
    }

    protected boolean hitAbove(GameObject og) {
        return !(this.x + this.width < og.getX() + 5 || this.x > og.getX() + og.getWidth() - 5 ||
                this.y < og.getY() + og.getHeight() || this.y > og.getY() + og.getHeight() + 5);
    }

    protected boolean hitAhead(BasicCharacter pers) {
        return this.isToRight() &&
                !(this.x + this.width < pers.getX() || this.x + this.width > pers.getX() + 5 || this.y + this.height <= pers.getY() || this.y >= pers.getY() + pers.getHeight());
    }

    protected boolean hitBack(BasicCharacter pers) {
        return !(this.x > pers.getX() + pers.getWidth() || this.x + this.width < pers.getX() + pers.getWidth() - 5 ||
                this.y + this.height <= pers.getY() || this.y >= pers.getY() + pers.getHeight());
    }

    public boolean hitBelow(BasicCharacter pers) {
        return !(this.x + this.width < pers.getX() || this.x > pers.getX() + pers.getWidth() ||
                this.y + this.height < pers.getY() || this.y + this.height > pers.getY());
    }

    public boolean isNearby(BasicCharacter pers) {
        return ((this.x > pers.getX() - PROXIMITY_MARGIN && this.x < pers.getX() + pers.getWidth() + PROXIMITY_MARGIN)
                || (this.x + this.width > pers.getX() - PROXIMITY_MARGIN && this.x + this.width < pers.getX() + pers.getWidth() + PROXIMITY_MARGIN));
    }

    public boolean isNearby(GameObject obj) {
        return ((this.x > obj.getX() - PROXIMITY_MARGIN && this.x < obj.getX() + obj.getWidth() + PROXIMITY_MARGIN) ||
                (this.getX() + this.width > obj.getX() - PROXIMITY_MARGIN && this.x + this.width < obj.getX() + obj.getWidth() + PROXIMITY_MARGIN));
    }
}
